## Запуск окружения:
* копируем папки проектов внутрь project/apps
* в папке nginx  копируем нужный файл 
    * exemple-service.conf 
* переименовываем файл под название проекта пример `project.conf`
* в файле заменям <srvice> на название папки проекта, пример `project`
* на Windows Windows\System32\drivers\etc в файл hosts добавляем значения
    * 127.0.0.1 project.test 
* собираем `docker-compose build`
* запускаем `docker-compose up -d` - можно запустить без ключа `-d`, тогда логи всех контейнеров будут падать в консоль
* открываем контейнер `docker exec -ti php sh`
* переходим в папку с проектом запускаем `composer install` (или php -d memory_limit=-1 /usr/local/bin/composer install -n --ignore-platform-reqs)

##все проект собран
* проект доступен по адресу project.test

## Настройки бд
* в конфигах проектов адрес mysql сервера должен быть `mysqldb` логин `root` пароль `testdb` (вместо localhost)
* для postgres логин `postgres` пароль `secret`

COMPOSER_PROCESS_TIMEOUT=2000 php composer.phar install -n --ignore-platform-reqs